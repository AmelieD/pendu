<!doctype html>
<html>
<head>
	<title>Pendu</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
	<style>
	a,
	button {
		margin-top:5px;
	}
	</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.php">CRUD Mots</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-6">
				<?php
				session_start();
				$alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ-";
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-xs-offset-2">

				<?php
				if (isset($_POST['game'])&& $_POST['game']=="Jouer") {
					include 'randomizer_pendu.php';
					$_SESSION['mot']=strtoupper($string);
					$_SESSION['img']=-1;
					for($i=0;$i<strlen($_SESSION['mot']);$i++) {
						$_SESSION['recherche'][$i]="_";
						$nbl++;
					}
					$_SESSION['nb_lettre']=$nbl;
				}
				if( (!empty($_GET['reset']) && $_GET['reset']=='true') || empty($_SESSION['mot']) ) {
					echo "<form method='post' action='pendu.php'>";
					echo "<input type='submit' name='game' value='Jouer'>";
					echo "</form>";
					session_destroy();
				}
				if (isset($_SESSION['mot'])) {
					if (isset($_GET['lettre'])) {
						$_SESSION['lettres'].=$_GET['lettre'];
						for($j=0;$j<count($_SESSION['recherche']);$j++) {
							if(substr($_SESSION['mot'],$j,1)==$_GET['lettre']) {
								$_SESSION['recherche'][$j]=$_GET['lettre'];
								$vrais='2';
								$_SESSION['nb_lettre']--;
							}
						}
					}
					for($i=0;$i<count($_SESSION['recherche']);$i++) {
						echo $_SESSION['recherche'][$i]." ";
					}
					echo "<br/>";

					if ($_SESSION['nb_lettre']==0) {
						echo 'Bravo vous avez gagné<br/><a href="pendu.php?reset=true">Recommencer</a>';
					} elseif ($_SESSION['img']==9) {
						echo 'Désolé vous avez perdu<br>Mot à trouver : '.$_SESSION['mot'].'<br><a href="pendu.php?reset=true">Recommencer</a>';
						$lost_on = $_SESSION['img'];
						session_unset();
					} else {
						for($i=0;$i<strlen($alphabet);$i++) {
							if (strpos($_SESSION['lettres'],substr($alphabet,$i,1))===false) {
								echo "<a class='btn btn-default' href='pendu.php?lettre=".substr($alphabet,$i,1)."'>".substr($alphabet,$i,1)."</a> ";
							} else {
								echo "<button class='btn btn-warning'>".substr($alphabet,$i,1)."</button> ";
							}
						}
					}

					if($vrais!=2) {
						$_SESSION['img']++;
					}

					echo "</div>";
					echo "<div class='col-xs-2'>";
					if(empty($lost_on)){
						echo '<img src="img/'.$_SESSION['img'].'.png" width="150" height="150"><br>';
					}
					else{
						echo '<img src="img/'.$lost_on.'.png" width="150" height="150"><br>';
					}
					$vrais='';

				}
				?>
			</div>
		</div>
	</div>

</body>
</html>
